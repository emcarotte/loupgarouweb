import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Role } from '../../../../common/Joueur';
import * as utils from '../services/fontionsUtiles';

@Component({
  selector: 'app-menu-roles',
  templateUrl: './menu-roles.component.html',
  styleUrls: ['./menu-roles.component.css']
})
export class MenuRolesComponent implements OnInit {

  nomsRoles: string[] = [];
  roleChoisi?: Role;
  images: string[] = [];

  constructor(private router: Router) {
    for(let i = 0; i<Object.keys(Role).length / 2; i++){
      this.nomsRoles.push(utils.convertirRoleTexte(i as Role, true));
      this.images.push(utils.imageRole(i));
    }
  }

  ngOnInit(): void {
    
  }

  descriptionRole(i: number){
    this.roleChoisi = i as Role;
  }

  fermerDescriptionRole(){
    this.roleChoisi = undefined;
  }

  retour(){
    this.router.navigate([""])
  }

}
