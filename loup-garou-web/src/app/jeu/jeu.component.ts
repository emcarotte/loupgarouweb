import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, HostListener, NgZone, OnInit, ViewChild } from '@angular/core';
import { CommunicationService } from '../services/communication.service';
import { SelecteurComponent } from '../selecteur/selecteur.component';
import { Joueur, Role } from '../../../../common/Joueur';
import { convertirRoleTexte } from '../services/fontionsUtiles';
import { InformationsComponent } from '../informations/informations.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-jeu',
  templateUrl: './jeu.component.html',
  styleUrls: ['./jeu.component.css'],
  animations:[
    trigger('parent', [
      transition(':enter', [])
    ]),
    trigger("evenements", [
      state('deplacement', style({marginLeft: "{{marginFinal}}px"}), {params : { marginFinal: 0}}),
      state('0', style({marginLeft: "0%"})),
      state('1', style({marginLeft: "-100%"})),
      state('2', style({marginLeft: "-200%"})),
      transition("0 <=> 1", [animate("0.2s")]),
      transition("0 <=> 2", [animate("0.2s")]),
      transition("1 <=> 2", [animate("0.2s")]),
      transition("deplacement => *", [animate("0.2s")])
    ]),
    trigger("village", [
      state('deplacement', style({marginLeft: "{{marginFinal}}px"}), {params : { marginFinal: 0}}),
      state('0', style({marginLeft: "100%"})),
      state('1', style({marginLeft: "0%"})),
      state('2', style({marginLeft: "-100%"})),
      transition("0 <=> 1", [animate("0.2s")]),
      transition("0 <=> 2", [animate("0.2s")]),
      transition("1 <=> 2", [animate("0.2s")]),
      transition("deplacement => *", [animate("0.2s")])
    ]),
    trigger("historique", [
      state('deplacement', style({marginLeft: "{{marginFinal}}px"}), {params : { marginFinal: 0}}),
      state('0', style({marginLeft: "200%"})),
      state('1', style({marginLeft: "100%"})),
      state('2', style({marginLeft: "0%"})),
      transition("0 <=> 1", [animate("0.2s")]),
      transition("0 <=> 2", [animate("0.2s")]),
      transition("1 <=> 2", [animate("0.2s")]),
      transition("deplacement => *", [animate("0.2s")])
    ])
  ]
})
export class JeuComponent implements OnInit {

  @ViewChild('parent') myDiv?: ElementRef;
  
  indexOnglet: number = 0;
  infoVillage: Joueur[] = [];
  proprieteFondEcran: string = "background-image: url(../../assets/fondEcran.png);";
  joueursVivants: Joueur[] = [];
  joueursMorts: Joueur[] = [];
  rolesVivants: string[] = [];
  rolesMorts: string[] = [];
  rolesVivantsEnum: Role[] = [];
  rolesMortsEnum: Role[] = [];
  historiqueEvenements: string[][] = [];

  
  entrainDeDrag: boolean = false;
  positionInitialeX: number = 0;
  marginFinal: number = 0;

  constructor(private ngZone: NgZone,public communicationService: CommunicationService, public router: Router) { }

  ngOnInit(): void {
  }

  evenementsOnglet(){
    this.indexOnglet = 0;
  }

  villageOnglet(){
    this.indexOnglet = 1;
  }

  historiqueEvenement(){
    this.indexOnglet = 2;
  }

  getHeight(): string{
    if(this.communicationService.isMeneurDeJeu){
      return "height:100%;";
    }else {
      return "height:93%;";
    }
  }

  
  onActivate(component: any){
    if(component instanceof SelecteurComponent){
      this.communicationService.getInfoVillage().subscribe((infos: Joueur[])=>{
        this.infoVillage = infos;
      })
      this.communicationService.getInfoVillageVerite().subscribe((infoVillage: Joueur[])=>{
        this.joueursVivants = infoVillage;
        this.rolesVivantsEnum = this.joueursVivants.map((joueur: Joueur)=>{
          return joueur.role!
        }).sort((a,b) => a-b);
        this.rolesVivants = this.rolesVivantsEnum.map((role: Role)=>{
          return convertirRoleTexte(role);
        })
      })
      this.communicationService.getInfoVillageMort().subscribe((infoVillageMort: Joueur[])=>{
        this.joueursMorts = infoVillageMort;
        this.rolesMortsEnum = this.joueursMorts.map((joueur: Joueur)=>{
          return joueur.role!;
        }).sort(((a,b) => a-b));
        this.rolesMorts = this.rolesMortsEnum.map((role: Role)=>{
          return convertirRoleTexte(role);
        })
      })
    }
    if(component instanceof InformationsComponent){
      this.communicationService.getHistoriqueEvenements().subscribe((historique: string[][])=>{
        this.historiqueEvenements = historique;
      })
    }
    this.ngZone.runOutsideAngular(()=>{
      let nouvellePropriete: string = "";
      if(component.urlImage){
        nouvellePropriete += "background-image: url("+component.urlImage+");"
        if(component.urlImage == "../../assets/soleilTentative8.jpg"){
          nouvellePropriete+="color: black;"
        }
        if(component.urlImage == "../../assets/soleilTentative0.jpg"){
          nouvellePropriete+="color: white;"
        }
      }
      if(component.couleurFond){
        nouvellePropriete += "background-color:"+component.couleurFond+";"
      }
      if(nouvellePropriete == ""){
        if(this.communicationService.jour){
          nouvellePropriete += "background-image: url('../../assets/soleilTentative8.jpg');"
          nouvellePropriete+="color: black;"
        } else {
          nouvellePropriete += "background-image: url('../../assets/soleilTentative0.jpg');"
          nouvellePropriete+="color: white;"
        }
      }
      this.proprieteFondEcran = nouvellePropriete;
    })
  }

  mouseDown(event: any){
    this.entrainDeDrag = true;
    if(event.clientX){
      this.positionInitialeX = event.clientX;
    } else {
      this.positionInitialeX = event.touches[0].clientX;
    }
  }
  
  mouseMove(event: any){
    if(this.entrainDeDrag){
      if(event.clientX){
        this.marginFinal = event.clientX-this.positionInitialeX;
      } else {
        this.marginFinal = event.touches[0].clientX-this.positionInitialeX;
      }
    }
  }

  mouseUp(){
    this.entrainDeDrag = false;
    if(this.marginFinal < -(this.myDiv!.nativeElement.offsetWidth/3) && this.indexOnglet < 2){
      this.indexOnglet++;
    }
    if(this.marginFinal > (this.myDiv!.nativeElement.offsetWidth/3) && this.indexOnglet > 0){
      this.indexOnglet--;
    }
    this.marginFinal = 0;
  }

  @HostListener('window:popstate', ['$event'])
  onPopState(event: any) {
    console.log("allooooo");
    window.history.forward();
    this.router.navigate(["creationComponent"])
  }

}
