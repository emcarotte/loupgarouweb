import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Role } from '../../../../common/Joueur';
import * as utils from '../services/fontionsUtiles';

@Component({
  selector: 'app-description-role',
  templateUrl: './description-role.component.html',
  styleUrls: ['./description-role.component.css']
})
export class DescriptionRoleComponent implements OnInit {

  nomRole: string = "";
  descriptionRole: string = "";
  imageRole: string = "";

  @Input() role?: Role;
  @Input() modeVillageoisVillageois: boolean = false;
  @Output() fermer = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
    if(this.modeVillageoisVillageois){
      this.nomRole = "Mode Villageois-Villageois";
      this.descriptionRole = "Le mode Villageois-Villageois laisse en vie la premiere victime des loup-garous, en lui retirant son rôle, le transformant en villageois-villageois."
      this.imageRole = "villageois.png";
    } else{
      this.nomRole = utils.convertirRoleTexte(this.role!, true);
      this.descriptionRole = utils.descriptionRole(this.role!);
      this.imageRole = utils.imageRole(this.role!);
    }
  }

  retour(): void {
    this.fermer.emit();
  }

}
