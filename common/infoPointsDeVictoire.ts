export interface InfoPointsDeVictoire{
    nom: string,
    pointsGagnes: number,
    points: number,
    idAppareil: number,
    idJoueur: number
}