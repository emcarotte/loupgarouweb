export interface InfoVideo{
    nomJoueursMorts: string[];
    nomJoueurAMontrer: string;
    textePendantLaNuit: string;
    texteJourSeLeve: string;
}