import { Role } from "../../../../common/Joueur";
import { Appareil } from "../appareil";
import { Partie } from "../partie";
import { ServanteDevouee } from "../Personnages/servanteDevouee";
import { Action, HistoriquePartie } from "./historiquePartie";

let historiquePartie: HistoriquePartie = JSON.parse(
    '{"actions":[[5,0],[5,0],[4,0],[1,4,0,2],[5,0],[0],[5,0],[5,0],[0],[5,0],[5,0],[0],[5,0],[5,0],[0],[5,0],[5,0],[0]],"choixPersonnages":[3,4,5,6,7,8,9,10,11,12,13,14,15,16,18,19,20],"nbAppareils":1,"nbJoueurs":21,"nbLoups":1,"seed":469,"modePatateChaude":false,"modeVillageoisVillageois":true,"histoire":false,"modeVideo":false,"meneurDeJeu":false,"noms":[["Joueur 0"]],"points":[[0]]}'
)
    
    
let partie: Partie = new Partie("0", historiquePartie.seed);
for(let i: number = 0; i < historiquePartie.nbAppareils-1; i++){
    partie.appareils.push(new Appareil(""+i, "Joueur "+i));
}
if(historiquePartie.meneurDeJeu){
    partie.appareils.push(new Appareil(""+(partie.appareils.length-1), "Joueur "+(partie.appareils.length-1)));
    partie.appareils[0].switchMeneurDeJeu();
}
partie.appareils.forEach((appareil: Appareil, i: number)=>{
    appareil.nomsJoueurs = historiquePartie.noms[i];
})

partie.choixPersonnages = historiquePartie.choixPersonnages;
partie.setNbJoueurs(historiquePartie.nbJoueurs, false);
partie.setNbLoups(historiquePartie.nbLoups, false);
partie.modePatateChaude = historiquePartie.modePatateChaude;
partie.modeVillageoisVillageois = historiquePartie.modeVillageoisVillageois;
partie.modeVideo = historiquePartie.modeVideo;
console.log("steaaaak", partie.appareils[0].nomsJoueurs);

partie.commencerPartie().then(async ()=>{
    for(let i: number = 0; i<historiquePartie.actions.length; i++){
        await envoyerDansSwitch(historiquePartie.actions[i], i);
    }
})

async function envoyerDansSwitch(action:number[], index: number){
    switch(action[0]){
        case Action.PROCHAINE_ETAPE:{
            console.log("prochaine étape " + index);
            await partie.prochaineEtape().then(()=>{
            });
            break;
        }
        case Action.VOTER_VILLAGEOIS:{
            console.log("faire une action: "+ action)
            partie.joueursVivants[action[2]].choisirJoueur(partie.joueursVivants[action[3]], action[1], false);
            break;
        }
        case Action.PASSER:{
            partie.appareils[action[1]].passer = true;
            break;
        }
        case Action.OUI_SERVANTE_DEVOUEE:{
            (partie.getPersonnages(Role.SERVANTE_DEVOUEE)[0] as ServanteDevouee).ouiVeutPrendrePersonnage();
            break;
        }
        case Action.POP_RAISON_PAS_VOTER:{
            console.log("pop raisons pas voter de index joueur" +action[1])
            partie.joueursVivants[action[1]].popRaisonsPasVoter();
            break;
        }
        case Action.GET_UN_EVENEMENT:{
            partie.appareils[action[1]].getUnEvenement();
            break;
        }
        default:
            console.log(partie.joueursVivants.map((joueur=>joueur.nom)));
            throw new Error("repere");
    }
}

