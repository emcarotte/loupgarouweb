import { Role } from "../../../../common/Joueur";
import { Partie } from "../partie";
import { Villageois } from "./villageois";

export class TroisFreres extends Villageois{

    deuxFreres: Villageois[];

    constructor(partie: Partie){
        super(false, partie);
        this.role = Role.TROIS_FRERES;
        this.deuxFreres = [];
    }

    actionIntro(): void {
        super.actionIntro();
        this.partie.joueursVivants.forEach((joueur: Villageois)=>{
            if(joueur.role == Role.TROIS_FRERES && joueur != this){
                this.deuxFreres.push(joueur);
            }
        })
    }

    annulerRole(servanteAPrisSonRole: boolean): void {
        super.annulerRole(servanteAPrisSonRole);
        this.deuxFreres.forEach((frere: Villageois)=>{
            (frere as TroisFreres).deuxFreres.splice((frere as TroisFreres).deuxFreres.indexOf(this), 1);
        })
    }

    actionExServante(): void {
        super.actionExServante();
        this.actionIntro();
    }
}