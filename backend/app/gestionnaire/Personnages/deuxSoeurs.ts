import { Role } from "../../../../common/Joueur";
import { Partie } from "../partie";
import { Villageois } from "./villageois";

export class DeuxSoeurs extends Villageois{

    deuxiemeSoeur?: Villageois;

    constructor(partie: Partie){
        super(false, partie);
        this.role = Role.DEUX_SOEURS;
    }

    actionIntro(): void {
        super.actionIntro();
        this.partie.joueursVivants.forEach((joueur: Villageois)=>{
            if(joueur.role == Role.DEUX_SOEURS && joueur != this){
                this.deuxiemeSoeur = joueur;
            }
        })
    }

    annulerRole(servanteAPrisSonRole: boolean): void {
        super.annulerRole(servanteAPrisSonRole);
        if(this.deuxiemeSoeur){
            (this.deuxiemeSoeur as DeuxSoeurs).deuxiemeSoeur = undefined;
        }
    }

    actionExServante(): void {
        super.actionExServante();
        this.actionIntro();
    }
}